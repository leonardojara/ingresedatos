<%-- 
    Document   : salida
    Created on : 29-mar-2020, 12:25:56
    Author     : Leonardo.Jara
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Bienvenido(a) </h1>
        
        <% 
        String nombre = (String) request.getAttribute("nombre");
        String seccion = (String) request.getAttribute("seccion");
         
        %>
        
        <p>Hola <%=nombre%>, su sección es la N° <%=seccion%></p>
          
        
    </body>
</html>
